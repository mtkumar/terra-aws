resource "aws_iam_user" "mt" {
    name = each.value
    for_each = toset(var.user_name)
}

resource "aws_iam_access_key" "mt" {
  user = aws_iam_user.mt[each.value].name
  for_each = toset(var.user_name)
}

resource "aws_iam_user_login_profile" "mt" {
  user    = aws_iam_user.mt[each.value].name
  for_each = toset(var.user_name)
  password_length = 10
  password_reset_required = true
}

data "aws_iam_policy" "admin" {
  arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_group" "developers" {
  name = "developers"
}

resource "aws_iam_user_group_membership" "adduser" {
  user = aws_iam_user.mt[each.value].name
  for_each = toset(var.user_name)

  groups = [
    aws_iam_group.developers.name,
  ]
}

resource "aws_iam_group_policy_attachment" "admin-attach" {
  group       = aws_iam_group.developers.name
  policy_arn = data.aws_iam_policy.admin.arn
}

output "password" {
  value = "[${aws_iam_user_login_profile.mt.*.password}]"
}
